Run Application
-----------
```
1. install venv with: python3 -m venv ./venv
2. enter venv with: source ./venv/bin/activate
3. While in Venv, install Django with: pip install django
4. While in venv: django-admin startproject btre . 
5. Start server with: python manage.py runserver 11434
```

Application Details
-----------
This Application is built with python and Django.


Application Details
-----------
To clone, run the following command:
git clone https://SpangleWebDesign@bitbucket.org/SpangleWebDesign/djangotrial.git

Make Migrations / Migrate
-----------
1. Create a new app
2. add app to installed apps
3. run command once model is set up: python manage.py makemigrations <addname>
4. Add table to database with: python manage.py migrate